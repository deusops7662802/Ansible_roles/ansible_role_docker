Ansible role Docker
=========
Роль устанавливает докер на хосте

Пример Playbook
----------------


    - hosts: servers
      roles:
         - docker



Variables
---------

Using variables in Ansible role Docker
- удалить существующую версию докера?

    - remove_docker_exist: false

- Для указания конкретной версии требуется заполнить переменную как в примере ниже
  
  - docker_version:"5:25.0.3-1\~ubuntu.20.04~focal"
  
  или
  
  - docker_version: "5:25.0.3*"

- Если требуется установить версию latest, тогда закоментировать строки с версией докера
  
  - #docker_version: "5:25.0.3-1\~ubuntu.20.04~focal"
  
  - #docker_version: "5:25.0.3*"


License
-------

FOSS
